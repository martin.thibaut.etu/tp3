import Page from './Page.js';

export default class AddPizzaPage extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		super.mount(element);
		const button = document.querySelector('.pizzaForm button');
		button.addEventListener('click', event => {
			event.preventDefault();
			this.submit();
		});
	}

	submit(event) {
		const input = document.querySelector('.pizzaForm input');
		if (input.value.length == 0) {
			window.alert('MET UN NOM DE PIZZA CHIEN');
		} else {
			//console.log(input.value);
			window.alert(`LA PIZZA ${input.value} A ETE AJOUTE`);
		}
	}
}

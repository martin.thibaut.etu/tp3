import Router from './Router';
import data from './data';
import Component from './components/Component';
import PizzaList from './pages/PizzaList';
import PizzaForm from './pages/PizzaForm';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList([]);
Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data;
Router.navigate('/'); // affiche la liste des pizzas

//////////////////////////////////////////////////////////////

//PARTIE B
const napo = document.querySelectorAll('.pizzaThumbnail h4')[1];
console.log(napo.innerHTML);

const title = document.querySelector('header nav a');
title.innerHTML = title.innerHTML + "<small>les pizzas c'est la vie</small>";

const secondLink = document.querySelectorAll('footer div a')[1];
console.log(secondLink.getAttribute('href'));

const active = document.querySelector('.pizzaListLink');
active.setAttribute('class', active.getAttribute('class') + ' active');

//PARTIE C
const display = document.querySelector('.newsContainer');
display.setAttribute('style', '');

function onButtonClick(event) {
	event.preventDefault();
	display.setAttribute('style', 'display:none');
}
const link = document.querySelector('.closeButton');
link.addEventListener('click', onButtonClick);

const aboutPage = new Component('section', null, 'Ce site est génial');
/*const pizzaForm = new Component(
	'section',
	null,
	'Ici vous pourrez ajouter une pizza'
);*/
const pizzaForm = new PizzaForm();

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.menuElement = document.querySelector('.mainMenu');

//PARIE D
